<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class user_middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        // if (Auth::user()->role == 'ADMIN') {
        //     return redirect()->route('admin');
        // }

        if (Auth::user()->role == 'USER') {
            return $next($request);
        }    
    }
}
