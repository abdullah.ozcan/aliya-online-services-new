@extends('layouts.menu')

@section('title')
About Us : E-Kirana
@endsection

@section('content')

<div class="breadcrumb section pt-60 pb-60">
    <div class="container">
      <h1 class="uppercase">About Us</h1>
      <ul>
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li class="active">About Us</li>
      </ul>
    </div>
  </div>
  <!-- =====  BREADCRUMB END===== -->
  <div class="page-about section">
  <!-- =====  CONTAINER START  ===== -->
  <div class="container">
    <div class="row ">        
      
      <div class="col-lg-12 col-xl-12 mb-20">
        <!-- about  -->
        <div class="row">
         
          <div class="col-md-12">
            <div class="about-text">
              <div class="about-heading-wrap">
                <h2 class="about-heading mb-20 mt-20 py-2">What is <span>E-Kinara? </span></h2>
              </div>
              <p>
                  
                  Bihar’s largest online food and grocery store. With over 1,000 products and over a 100 brands in our catalogue you will find everything you are looking for. Right from fresh Fruits and Vegetables, Rice and Dals, Spices and Seasonings to Packaged products, Beverages, Personal care products, Meats – we have it all.<br>
Choose from a wide range of options in every category, exclusively handpicked to help you find the best quality available at the lowest prices. Select a time slot for delivery and your order will be delivered right to your doorstep, You can pay online using your debit / credit card or by cash / sodexo on delivery.<br>
We guarantee on time delivery, and the best quality!<br>
<h3><b>Happy Shopping</b></h3>

              </p>
            </div>
          </div>
          
              <div class="col-md-12">
            <div class="about-text">
              <div class="about-heading-wrap">
                <h2 class="about-heading mb-20 mt-20 py-2">Why should I use <span>E-Kinara? </span></h2>
              </div>
              <p>
                  
                  E-Kinara allows you to walk away from the drudgery of grocery shopping and welcome an easy relaxed way of browsing and shopping for groceries. Discover new products and shop for all your food and grocery needs from the comfort of your home or office. No more getting stuck in traffic jams, paying for parking, standing in long queues and carrying heavy bags – get everything you need, when you need, right at your doorstep. Food shopping online is now easy as every product on your monthly shopping list, is now available online at bigbasket.com, Bihar’s best online grocery store.

              </p>
            </div>
          </div>
        </div>
        <!-- =====  What We Do? ===== -->
       
        <!-- =====  end  ===== -->
      <!--  <div class="team-section section mt-40">-->
        <!--Team Carousel -->
      <!--  <div class="heading-part mb-10">-->
      <!--    <h3 class="section_title mt-50">Our Creative Team</h3>-->
      <!--  </div>-->
      <!--  <div class="team_grid row-10">-->
      <!--    <div class="team3col  owl-carousel">-->
      <!--      <div class="item team-detail">-->
      <!--        <div class="team-item-img"> <img src="use_assets/images/tm1.jpg" alt="" /> </div>-->
      <!--        <div class="team-designation mt-20">php Developer</div>-->
      <!--        <h4 class="team-title  my-2">joseph Lui </h4>-->
      <!--        <p>Lorem ipsum dolor sit amet, sea in odio erat, volumu Clita prodesset Rem ipsum dolor s..</p>-->
      <!--        <ul class="social mt-20 mb-80">-->
      <!--          <li><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>-->
      <!--          <li><a href="https://www.twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>-->
      <!--          <li><a href="https://www.dribbble.com/" target="_blank"><i class="fab fa-dribbble"></i></a></li>-->
      <!--          <li><a href="https://www.pinterest.com/" target="_blank"><i class="fab fa-pinterest"></i></a></li>-->
      <!--          <li><a href="https://www.behance.net/" target="_blank"><i class="fab fa-behance"></i></a></li>-->
      <!--        </ul>-->
      <!--      </div>-->
      <!--      <div class="item team-detail">-->
      <!--        <div class="team-item-img"> <img src="use_assets/images/tm2.jpg" alt="" /> </div>-->
      <!--        <div class="team-designation mt-20">php Developer</div>-->
      <!--        <h4 class="team-title  my-2">joseph Lui </h4>-->
      <!--        <p>Lorem ipsum dolor sit amet, sea in odio erat, volumu Clita prodesset Rem ipsum dolor s..</p>-->
      <!--        <ul class="social mt-20 mb-80">-->
      <!--          <li><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>-->
      <!--          <li><a href="https://www.twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>-->
      <!--          <li><a href="https://www.dribbble.com/" target="_blank"><i class="fab fa-dribbble"></i></a></li>-->
      <!--          <li><a href="https://www.pinterest.com/" target="_blank"><i class="fab fa-pinterest"></i></a></li>-->
      <!--          <li><a href="https://www.behance.net/" target="_blank"><i class="fab fa-behance"></i></a></li>-->
      <!--        </ul>-->
      <!--      </div>-->
      <!--      <div class="item team-detail">-->
      <!--        <div class="team-item-img"> <img src="use_assets/images/tm3.jpg" alt="" /> </div>-->
      <!--        <div class="team-designation mt-20">php Developer</div>-->
      <!--        <h4 class="team-title  my-2">joseph Lui </h4>-->
      <!--        <p>Lorem ipsum dolor sit amet, sea in odio erat, volumu Clita prodesset Rem ipsum dolor s..</p>-->
      <!--        <ul class="social mt-20 mb-80">-->
      <!--          <li><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>-->
      <!--          <li><a href="https://www.twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>-->
      <!--          <li><a href="https://www.dribbble.com/" target="_blank"><i class="fab fa-dribbble"></i></a></li>-->
      <!--          <li><a href="https://www.pinterest.com/" target="_blank"><i class="fab fa-pinterest"></i></a></li>-->
      <!--          <li><a href="https://www.behance.net/" target="_blank"><i class="fab fa-behance"></i></a></li>-->
      <!--        </ul>-->
      <!--      </div>-->
      <!--      <div class="item team-detail">-->
      <!--        <div class="team-item-img"> <img src="use_assets/images/tm4.jpg" alt="" /> </div>-->
      <!--        <div class="team-designation mt-20">php Developer</div>-->
      <!--        <h4 class="team-title  my-2">joseph Lui </h4>-->
      <!--        <p>Lorem ipsum dolor sit amet, sea in odio erat, volumu Clita prodesset Rem ipsum dolor s..</p>-->
      <!--        <ul class="social mt-20 mb-80">-->
      <!--          <li><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>-->
      <!--          <li><a href="https://www.twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>-->
      <!--          <li><a href="https://www.dribbble.com/" target="_blank"><i class="fab fa-dribbble"></i></a></li>-->
      <!--          <li><a href="https://www.pinterest.com/" target="_blank"><i class="fab fa-pinterest"></i></a></li>-->
      <!--          <li><a href="https://www.behance.net/" target="_blank"><i class="fab fa-behance"></i></a></li>-->
      <!--        </ul>-->
      <!--      </div>-->
      <!--      <div class="item team-detail">-->
      <!--        <div class="team-item-img"> <img src="use_assets/images/tm5.jpg" alt="" /> </div>-->
      <!--        <div class="team-designation mt-20">php Developer</div>-->
      <!--        <h4 class="team-title  my-2">joseph Lui </h4>-->
      <!--        <p>Lorem ipsum dolor sit amet, sea in odio erat, volumu Clita prodesset Rem ipsum dolor s..</p>-->
      <!--        <ul class="social mt-20 mb-80">-->
      <!--          <li><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>-->
      <!--          <li><a href="https://www.twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>-->
      <!--          <li><a href="https://www.dribbble.com/" target="_blank"><i class="fab fa-dribbble"></i></a></li>-->
      <!--          <li><a href="https://www.pinterest.com/" target="_blank"><i class="fab fa-pinterest"></i></a></li>-->
      <!--          <li><a href="https://www.behance.net/" target="_blank"><i class="fab fa-behance"></i></a></li>-->
      <!--        </ul>-->
      <!--      </div>-->
      <!--    </div>-->
          <!--End Team Carousel -->
      <!--  </div>-->
      <!--</div>-->
      </div>
    </div>
  </div>
  <hr>
</div>

@endsection