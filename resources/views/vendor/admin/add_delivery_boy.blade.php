@extends('admin.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Add New Delivery Boy</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-body">
                                            <form method="post" action="add_delivery_boy_action" autocomplete="off" >
    @csrf
											<div class="row">
												<div class="col-md-6">
													<div class="form-group ">
                                                    <label>Name</label>
                                                        <input type="text" class="form-control" name="name" placeholder="Enter Delivery Boy Name"  value="{{ old('name') }}" autocomplete="nope">
                                                        <span class="text-danger">{{ $errors->first('name') }}</span>
													</div>
                                                </div>
                                                <div class="col-md-6">
													<div class="form-group ">
                                                    <label>Contact No</label>
                                                    <input type="number" class="form-control" name="mobile" placeholder="Enter Delivery Boy Contact No"  value="{{ old('mobile') }}" autocomplete="nope">
                                                    <span class="text-danger">{{ $errors->first('mobile') }}</span>
													</div>
                                                </div>
                                                <div class="col-md-12">
													<div class="form-group ">
                                                    <label>Email ID</label>
                                                    <input type="email" class="form-control" name="email" placeholder="Enter Delivery Boy Email ID"  value="{{ old('email') }}" autocomplete="nope">
                                                    <span class="text-danger">{{ $errors->first('email') }}</span>
													</div>
                                                </div>
											
                                              
                                              
                                                <div class="col-md-12">
												    <div class="form-group ">
														<label>Select Delivery Pincodes</label>
                                                        <select class="form-control select2 w-100" multiple="multiple" data-placeholder="Select a Pincode" name="pincode[]" autocomplete="nope">
                                                            @foreach($pin as $pin)
                                                            <option value="{{$pin->pincode}}">{{$pin->pincode}}</option>
                                                            @endforeach
															
                                                        </select>
                                                        <span class="text-danger">{{ $errors->first('pincode') }}</span>
                                                    	</div>
                                                </div>
                                                <div class="col-md-12">
                                                    <br>
                                                    <center><input type="submit" class="btn btn-primary mt-1 mb-1" value="Save"></center>
												</div>
                                               
                                            </div>
                                            </form>
										</div>
									</div>
								
								</div>

								
							</div>
							<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	

@endsection