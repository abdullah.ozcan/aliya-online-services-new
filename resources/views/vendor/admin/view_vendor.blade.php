@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
    <div class="page-header mt-0  p-3">
        <h3 class="mb-sm-0">View Vendor</h3>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
            <div class="card-header">
         
                                    <h2 class="mb-0">View Vendor</h2>
                         
                
                </div>
                <div class="card-body">
                    <div class="table-responsive">

                        <table id="example" class="table table-striped table-bordered w-100 text-nowrap">
                            <thead>
                                <tr>
                                    <th class="wd-15p">Register <br>Date</th>
                                    <th class="wd-15p">Vendor <br>Name</th>
                                    <th class="wd-15p">Vendor <br>Contact No</th>
                                    <th class="wd-15p">Vendor <br>Email</th>
                                    <th>Approval</th>
                                    <th>Block Status</th>
                                    <th class="wd-15p">Action</th>     
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($vendor as $vendor)
                                <tr>
                                    <td>{{$vendor->created_at}}</td>
                                    <td>{{$vendor->name}}</td>
                                    <td>{{$vendor->mobile}}</td>
                                    <td>{{$vendor->email}}</td>
                                    <td>
                                        @if ($vendor->status!='YES')
                                            <span class="badge badge-primary">Wait for approval</span>
                                            <a href="/approveStatus?id={{$vendor->id}}&status=NO" class="btn btn-info mt-1 mb-1 btn-sm">Approve</a>
                                        @else
                                            <span class="badge badge-primary">Approved</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($vendor->block=='NO')
                                            <span class="badge badge-primary">Active</span>
                                            <a href="/updateVendorBlockStatus?id={{$vendor->id}}&status=YES" class="btn btn-danger mt-1 mb-1 btn-sm">Block</a>
                                        @else
                                            <span class="badge badge-danger">Block</span>
                                            <a href="/updateVendorBlockStatus?id={{$vendor->id}}&status=NO" class="btn btn-info mt-1 mb-1 btn-sm">Unblock</a>
                                      
                                        @endif
                                    </td>
                                    <td> <a href="/viewDetails?id={{$vendor->id}}" class="btn btn-primary mt-1 mb-1 btn-sm">View Details</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                               
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script
    src="https://code.jquery.com/jquery-3.4.1.js"
    integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous">
</script>
    <script>
        $(document).ready(function() {
              

              $('#example').DataTable( {
                   "order": [[ 0, "desc" ]]
               } );
               
           });
                                       </script>


@endsection