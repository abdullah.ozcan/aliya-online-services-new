<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<meta name="description" content="Sell on ALIYA Online Shopping | Grow your business today with best online selling platform for e-commerce solutions.">



	<!-- Title -->
	<title>Sell on ALIYA Online Shopping | Grow your business today with best online selling platform for e-commerce solutions.</title>

	<!-- Favicon -->
	<link rel="icon" href="/img/fevicon/favicon48.png"/>
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800" rel="stylesheet">

	<!-- Icons -->
	<link href="../vendor_assets/css/icons.css" rel="stylesheet">

	<!--Bootstrap.min css-->
	<link rel="stylesheet" href="../vendor_assets/plugins/bootstrap/css/bootstrap.min.css">

	<!-- Adon CSS -->
	<link href="../vendor_assets/css/dashboard.css" rel="stylesheet" type="text/css">

	<!-- Single-page CSS -->
	<link href="../vendor_assets/plugins/single-page/css/main.css" rel="stylesheet" type="text/css">
	<link rel="icon" href="/img/fevicon/favicon.png"/>
</head>

<body class="bg-gradient-primary">


@yield('body')

<!-- Adon Scripts -->
	<!-- Core -->
	<script src="../vendor_assets/plugins/jquery/dist/jquery.min.js"></script>
	<script src="../vendor_assets/js/popper.js"></script>
	<script src="../vendor_assets/plugins/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>