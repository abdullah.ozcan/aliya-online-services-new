@extends('vendor.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0" style="color:red">  {{Session::get('message')}}
                                <a href="add_coupon" type="button" class="btn btn-primary mt-1 mb-1">Add Coupon</a></h3>
								<ol class="breadcrumb mb-0">
                                
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard         </li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Coupon Banners</h2>
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
															<th class="wd-15p">Coupon <br>Title</th>
                                                            <th class="wd-15p">Coupon <br>Image</th>
														
                                                            <th class="wd-15p">Coupon<br>Value</th>
															<th class="wd-15p">Coupon <br>Validity</th>

                                                            <th class="wd-15p">Coupon <br>Code</th>
															<th class="wd-15p">Minimum <br>Price(₹)</th>
                               
															<th class="wd-15p">Category <br>Name</th>
															 
														
														
															<th class="wd-20p">Action</th>
															
														</tr>
													</thead>
													<tbody>
													
													@foreach($coupon as $coupon)
														<tr>
                                                        
                                                        <td>{{$coupon->title}}</td>
                                                        <td><img src="../coupon_image/{{$coupon->coupon_image}}" style="height:100px"></td>
                                                        <td>{{$coupon->coupon_value}} @if($coupon->coupon_type=='FLAT') <span class="badge badge-pill badge-info">Flat Discount</span> @else <span class="badge badge-pill badge-primary">Percentage(%) Discount</span> @endif</td>
                                                        <td>{{$coupon->coupon_validity}}</td>
                                                        <td>{{$coupon->coupon_code}}</td>
                                                        <td>{{$coupon->min_price}}</td>

														<td>@if($coupon->cat_id!=0) 
														
														@php($cat=DB::table('cats')->where('cat_id',$coupon->cat_id)->first())
														{{$cat->cat_name}}
														@else <span class="badge badge-pill badge-danger">No Category</span> @endif</td>
														 	

															  <td><a href="update_coupon?coupon_id={{$coupon->id}}" class="btn btn-icon btn-pill btn-info mt-1 mb-1 btn-sm">update</a>
															  <a href="delete_coupon?coupon_id={{$coupon->id}}" class="btn btn-danger btn-pill btn-secondary mt-1 mb-1 btn-sm">Delete</a>
                                                      
																<br>
															<!--<a href="delete_coupon?coupon_id={{$coupon->id}}" class="btn btn-warning btn-pill btn-info mt-1 mb-1 btn-sm">Delete</a>-->
															
																 
																  </td>
                                                              

															
														</tr>

                                                        @endforeach
													
														
													</tbody>
												</table>
											
											</div>
												
										</div>
										
									</div>
								</div>
							</div>
							<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
							<script type="text/javascript">

$(document).ready(function() {
//sub category view
$('#today').click(function(){
    var product_id = $("#product_id").val();
    var token = $("#_token").val();
 alert(product_id);
$.ajax({

    url:'add_deals',

    type:'POST',

    data:{product_id:product_id,_token:token},
    dataType:"text",
  success:function(response)
    {
    
   
        alert('success');


  
    }

    });
});


});

</script>
@endsection
