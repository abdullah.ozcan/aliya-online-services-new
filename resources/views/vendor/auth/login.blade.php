@extends('vendor.layouts.loginmenu')

@section('body')
<body class="bg-gradient-primary">
	<div class="page">
		<div class="page-main">
			<div class="limiter">
				<div class="container-login100">
						
					<div class="wrap-login100 p-5">
						<form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
						@csrf
							<div class="logo-img text-center pb-3">
							<img src="/img/fevicon/favicon.png" alt="logo-img" height="50px">
							</div>
							<span class="login100-form-title">
								Vendor Login
							</span>

							<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
								<input class="input100" placeholder="Email" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required >
								
								
								<span class="symbol-input100">
									<i class="fa fa-envelope" aria-hidden="true"></i>
								</span>

								
							</div>
<input type="hidden" name='role' value="VENDOR">
							<div class="wrap-input100 validate-input" data-validate = "Password is required">
								<input class="input100" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
							
								@if ($errors->has('password'))
                                    <span class="focus-input100" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
								<span class="symbol-input100">
									<i class="fa fa-lock" aria-hidden="true"></i>
								</span>

								
							</div>
						
							<div class="container-login100-form-btn">
							<button type="submit" class="login100-form-btn btn-primary">
                                    {{ __('Login') }}
                                </button>
								
							</div>
						
							<div class="text-center pt-2">
							@if ($errors->has('email'))
                                
                                        <strong style="color:red">{{ $errors->first('email') }}</strong>
                                   
                                @endif
								<br>
								<div class="form-group form-forgot">
                                    @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Password?') }}
                                    </a>
                                @endif
                                
						<br>
								<a class="txt2" href="{{ route('vendor_register') }}">
									Not a member? Register Now
									<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
								</a>

								<br>
								<br>
                    @if(Session::has('message'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>	
                                        <strong>{{ Session::get('message') }}</strong>
                                </div>
                                @endif
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
	