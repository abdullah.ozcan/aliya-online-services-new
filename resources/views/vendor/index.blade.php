@extends('vendor.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0 p-3">
								<h3 class="mb-sm-0">Welcome to ALIYA Online Services Vendor Dashboard</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Vendor Dashboard</li>
								</ol>

							</div>
							<div class="row finance-content">
								<div class="col-xl-3 col-md-6">
									<div class="card shadow text-center">
										<div class="card-body">
											<h3 class="mb-3">Total Product</h3>
											<div class="chart-circle" data-value="1.0" data-thickness="10" data-color="#ad59ff"><canvas width="128" height="128"></canvas>
												<div class="chart-circle-value"><div class="text-xxl">{{$total_product}}</div></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xl-3 col-md-6">
									<div class="card shadow text-center">
										<div class="card-body">
											<h3 class="mb-3">Total Booking</h3>
											<div class="chart-circle" data-value="1.0" data-thickness="10" data-color="#00d9bf"><canvas width="128" height="128"></canvas>
												<div class="chart-circle-value"><div class="text-xxl">{{$total_booking}}</div></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xl-3 col-md-6">
									<div class="card shadow text-center">
										<div class="card-body">
											<h3 class="mb-3">Total Complete Booking</h3>
											<div class="chart-circle" data-value="1.0" data-thickness="10" data-color="#fc0"><canvas width="128" height="128"></canvas>
												<div class="chart-circle-value"><div class="text-xxl">{{$madiator}}</div></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xl-3 col-md-6">
									<div class="card shadow text-center">
										<div class="card-body">
											<h3 class="mb-3">Total Pending Booking</h3>
											<div class="chart-circle" data-value="1.0" data-thickness="10" data-color="#00b3ff"><canvas width="128" height="128"></canvas>
												<div class="chart-circle-value"><div class="text-xxl">{{$normal_user}}</div></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xl-9">
									<div class="card  shadow">
										<div class="card-header bg-transparent">
											<div class="row align-items-center">
												<div class="col-6">
													<h6 class="text-uppercase text-light ls-1 mb-1">Overview</h6>
													<h2 class="mb-0">Monthly Report</h2>
												</div>
												<div class="col-6">
													<h6 class="text-uppercase text-light ls-1 mb-1" style="float: right;    clear: both;">Total Product <i class="fas fa-square-full" style="color:rgb(173, 89, 255)"></i></h6>
													<h6 class="text-uppercase text-light ls-1 mb-1" style="float: right;    clear: both;">Total Register User <i class="fas fa-square-full" style="color:rgb(0, 217, 191) !important;"></i></h6>
													<h6 class="text-uppercase text-light ls-1 mb-1" style="float: right;    clear: both;">Total Successful Booking <i class="fas fa-square-full" style="color:rgb(255, 204, 0) !important;"></i></h6>
													
												
												</div>
											</div>
										</div>
										<div class="card-body">
											<!-- Chart -->
											<canvas id="finance-chartt" class="chart-dropshadow h-315"></canvas>
										</div>
									</div>
								</div>
								<div class="col-xl-3">
									<div class="">
										<div class="">
											<div class="row">
												<div class="col-xl-12">
													<div class="card shadow bg-gradient-primary">
														<div class="card-body">
															<div class="widget text-center">
																<small class="text-white h3">Total Pending Booking</small>
																<h2 class="text-xxl text-white mb-0">{{$pending_booking}}</h2>
															</div>
														</div>
													</div>
													<div class="card shadow bg-gradient-info">
														<div class="card-body">
															<div class="widget text-center">
																<small class="text-white h3">Total Cancel Booking</small>
																<h2 class="text-xxl text-white mb-0">{{$cancel_booking}}</h2>
															</div>
														</div>
													</div>
													<div class="card shadow bg-gradient-success">
														<div class="card-body">
															<div class="widget text-center">
																<small class="text-white h3">Total Return Booking</small>
																<h2 class="text-xxl text-white mb-0">{{$return_booking}}</h2>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<script src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
							<script type="text/javascript">

$(document).ready(function() {


	var ctx = document.getElementById("lineCharti");


var value1 = [
    
			@for($i=1;$i<13;$i++)
			
			@php($product =DB::table('products')->whereMonth('products.created_at',$i)->count())
			"{{$product}}", 

			@endfor
			

];

var value2 = [
    
	@for($i=1;$i<13;$i++)
			
			@php($users =DB::table('users')->whereMonth('users.created_at',$i)->count())
			"{{$users}}", 

			@endfor

];


var value3 = [
    
	@for($i=1;$i<13;$i++)
			
			@php($bookings =DB::table('bookings')->whereMonth('bookings.created_at',$i)->count())
			"{{$bookings}}", 

			@endfor

];

	var ctx = document.getElementById("finance-chartt");
	var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: ["Jan", "feb", "Mar", "Apr", "May", "Jun", "Jul","Aug","Sep","Oct","Nov","Dec"],
			type: 'line',
			defaultFontFamily: 'Montserrat',
			datasets: [{
				label: "Product",
				data: value1,
				backgroundColor: 'transparent',
				borderColor: '#ad59ff',
				borderWidth: 4,
				pointStyle: 'circle',
				pointRadius: 10,
				pointBorderColor: 'transparent',
				pointBackgroundColor: '#ad59ff',
			}, {
				label: "User",
				data: value2,
				backgroundColor: 'transparent',
				borderColor: '#00d9bf',
				borderWidth: 4,
				pointStyle: 'circle',
				pointRadius: 10,
				pointBorderColor: 'transparent',
				pointBackgroundColor: '#00d9bf',
			}, {
				label: "Booking",
				data: value3,
				backgroundColor: 'transparent',
				borderColor: '#fc0',
				borderWidth: 4,
				pointStyle: 'circle',
				pointRadius: 10,
				pointBorderColor: 'transparent',
				pointBackgroundColor: '#fc0',
			}]
		},
		options: {
			responsive: true,
			maintainAspectRatio: false,
			tooltips: {
				mode: 'index',
				titleFontSize: 12,
				titleFontColor: 'rgba(0,0,0,0.5)',
				bodyFontColor: 'rgba(0,0,0,0.5)',
				backgroundColor: '#fff',
				titleFontFamily: 'Montserrat',
				bodyFontFamily: 'Montserrat',
				cornerRadius: 3,
				intersect: false,
			},
			legend: {
				display: false,
				labels: {
					usePointStyle: true,
					fontFamily: 'Montserrat',
				},
			},
			scales: {
				xAxes: [{
					ticks: {
						fontColor: "rgba(0,0,0,0.5)",
					},
					display: true,
					gridLines: {
						color: 'rgba(0,0,0,0.061)'
					},
					scaleLabel: {
						display: true,
						labelString: 'Month',
						fontColor: 'rgba(0,0,0,0.61)'
					}
				}],
				yAxes: [{
					ticks: {
						fontColor: "rgba(0,0,0,0.5)",
					},
					display: true,
					gridLines: {
						display: false,
						drawBorder: true
					},
					scaleLabel: {
						display: true,
						labelString: 'Thousands',
						fontColor: 'rgba(0,0,0,0.61)'
					}
				}]
			},
			title: {
				display: false,
				text: 'Normal Legend'
			}
		}
	});
});

</script>
	


 

                        @endsection