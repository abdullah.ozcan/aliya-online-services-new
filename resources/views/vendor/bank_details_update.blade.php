@extends('vendor.layouts.vendor_menu')


@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Bank Details</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Vendor Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<form method="post" action="update_bank" enctype="multipart/form-data">
									<div class="card shadow">
										<div class="card-header">
										@csrf
										<div class="card-body">
                                        @foreach($bank as $bank)
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label class="form-label">Bank Name</label>
														<input type="text" name="bank_name" value="{{$bank->bank_name}}" required class="form-control"  placeholder="Bank name">
													</div>
													<div class="form-group">
														<label class="form-label">Branch Name</label>
														<input type="text" name="b_name" value="{{$bank->b_name}}" required class="form-control"  placeholder="Branch name">
													</div>
													<div class="form-group">
														<label class="form-label"> Account Holder Name</label>
														<input type="text" class="form-control" value="{{$bank->a_h_name}}"  name="cus_name" placeholder="Account Holder Name">
													</div>
													<div class="form-group">
														<label class="form-label"> Account No</label>
														<input type="text" class="form-control" value="{{$bank->a_no}}"  name="a_no" placeholder="Account Number">
													</div>
                                                    <div class="form-group">
														<label class="form-label">IFCS Code</label>
														<input type="text" class="form-control"  name="ifcs_code" value="{{$bank->ifcs_code}}" placeholder="IFCS Code">
													</div>
													<div class="form-group">
														<label class="form-label">UPI ID</label>
														<input type="text" class="form-control"  name="upi_id" value="{{$bank->upi_id}}" placeholder="UPI">
													</div>
                                                    
                                                    <div class="form-group">
                                                            <center><input type="submit" value="Save Changes" class="btn btn-icon  btn-primary mt-1 mb-1"></center>
                                                    </div>
												</div>
                                                @endforeach
                                           
											</div>
										</div>
									</div>
                                    </form>
								</div>

								
							</div>

@endsection