@extends('layouts.menu')

@section('title')
Wishlist : E-Kirana
@endsection

@section('content')
  <!-- =====  BREADCRUMB STRAT  ===== -->
  <div class="breadcrumb section pt-60 pb-60">
    <div class="container">
      <h1 class="uppercase">My Wishlist</h1>
      <ul>
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li class="active">My Wishlist</li>
      </ul>
    </div>
  </div>
  <!-- =====  BREADCRUMB END===== -->
  <!-- =====  HEADER END  ===== -->
  <div class="page-cart section">
  <!-- =====  CONTAINER START  ===== -->
    <div class="container">
      <div class="row ">
        <div class="col-12 mt-20 mb-20">
          <form enctype="multipart/form-data" method="post" action="#">
            <div class="table-responsive" ID="wishlist_poduct_view">
            
            </div>
          </form>
         
        </div>
      </div>
    <!-- =====  Brand start ===== -->
    <div id="brand_carouse" class="section text-center mt-30 pb-15">
        <div class="row">
          <div class="col-12">
            <div class="section_title">Our Popular Brands</div>
          </div>
          <div class="col-sm-12">
            <div class="brand owl-carousel">
              @php($brands=DB::table('brands')
              ->get())
            @foreach($brands as $brands)
              <div class="product-thumb"><div class="item text-center"> <a href="/shop?brand={{$brands->brand_id}}"><img src="/brand_logo/{{$brands->brand_image}}" title="{{$brands->brand_name}}" alt="Disney" class="img-responsive" /></a> </div></div>
            
              @endforeach         
            </div>
          </div>
        </div>
      </div>
    <!-- =====  Brand end ===== -->
    </div>
  <!-- =====  CONTAINER END  ===== -->
  </div>
  <script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous">
</script>
  <script type="text/javascript">

$(document).ready(function() {

wishlist_table();
});

function wishlist_table(){

var token = $("#_token").val();

$.ajax({

url:'wishlist_page_ajax',

type:'POST',

data:{_token:token},

success:function(response)
{


  $("#wishlist_poduct_view").html(response);

  sub_total();


}

});
}


</script>
@endsection
