@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Update Pincode</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
                                        <h2 class="mb-0">Update Pincode - {{$pin->pincode}}</h2>
										</div>
										<form method="POST" class="appointment-form" id="" action="update_pincode_code" role="form" name="frm">
										<div class="card-body">
                                        <div class="row">
											<div class="col-md-4"><label>Delivary Charge</label>
											<div class="form-group {{ $errors->has('delivary_charge') ? 'has-error' : '' }}" >
                                                    <input type="text" class="form-control" name="delivary_charge"  placeholder="Add Pin Code" value="{{$pin->delivery_charge}}">
													<span class="text-danger">{{ $errors->first('delivary_charge') }}</span>
											    </div>
                                                
                                        </div>
										
										<div class="col-md-4"><label>Minimum Order</label>
										<div class="form-group {{ $errors->has('minimum_order') ? 'has-error' : '' }}" >
                                                    <input type="text" class="form-control" name="minimum_order" placeholder="Add Delivary Charge" value="{{$pin->min_price}}">
													<span class="text-danger">{{ $errors->first('minimum_order') }}</span>
											    </div>
                                                
                                        </div>
										<div class="col-md-4"><label>COD</label>
										<div class="form-group {{ $errors->has('cod') ? 'has-error' : '' }}" >
                                                <select name="cod" id="cod" class="form-control">
														    <option value="" disable  >-- Select Option --</option>
															<option value="YES" @if($pin->cod=='YES') selected @endif>YES</option>
															<option value="NO" @if($pin->cod=='NO') selected @endif>NO</option>
														</select>
														<span class="text-danger">{{ $errors->first('cod') }}</span>
                                                </div>
                                                
                                        </div>
                                     
                                        <input type="hidden" name="pin_id" id="pin_id" value="{{$pin->id}}"/>
                                           
                                            <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
                                            <input type="submit" name="submit" class="btn btn-primary" id="submit" value="Save Changes"/>
                                    </div>
										
									</div>
									<center><span id="submit"></span></center>
								</div>

								</form>
											
								</div>
							</div>

							
</div>
							@endsection
