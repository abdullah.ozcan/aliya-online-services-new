@extends('delivery_boy.layouts.menu')
@section('title','Profile | E-Kirana')
@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
    .swal2-popup {
font-size: 2rem;
}
    </style>
<div class="content-wrapper" style="
    padding-top: 14px;
">
    <!-- Content Header (Page header) -->
  

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
         
            <!-- /.card -->

            <!-- About Me Box -->
      
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Change Password</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Edit Profile Details</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="card card-primary">
                        <div class="card-header">
                          <h3 class="card-title">Change Password</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                    <form  id="adr" enctype="multipart/form-data" action="{{route('delivery_change_password_password')}}">
                        @csrf
                          <div class="card-body">
                            
                            <div class="form-group">
                              <label for="exampleInputPassword1">Old Password</label>
                              <input type="password" name="current_password" class="form-control" id="current_password" placeholder="Password">
                            </div>
                            <div class="text-danger"><strong id="current_password_error"></strong></div>
                            <div class="form-group">
                                <label for="exampleInputPassword1"> New Password</label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                            </div>
                            <div class="text-danger"><strong id="password_error"></strong></div>

                              <div class="form-group">
                                <label for="exampleInputPassword1">Confirm Password</label>
                                <input type="password" class="form-control" name="confirm" id="confirm" placeholder="Password">
                              </div>
                              <div class="text-danger"><strong id="confirm_error"></strong></div>
                          
                          </div>
                          <!-- /.card-body -->
          
                          <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                          </div>
                        </form>
                      </div>
               
                    <!-- /.post -->

                    <!-- Post -->
                 
                    <!-- /.post -->

                    <!-- Post -->
                  
                    <!-- /.post -->
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">
                    <!-- The timeline -->
                    <div class="card card-success">
                        <div class="card-header">
                          <h3 class="card-title">Edit Profile</h3>
                        </div>
                    <form id="update_details" action="{{route('edit_profile')}}" method="post" enctype="multipart/form-data" >
                      @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{Auth::user()->name}}" placeholder="Enter Name">
                                <div class="text-danger"><strong id="name_error"></strong></div>
                            </div>
                       

                        
                          <div class="form-group">
                            <label for="exampleInputEmail1">Choose Profile Image </label>
                            <div class="input-group">
                                <div class="custom-file">
                                    
                                    <input type="file"  class="custom-file-input" id="profile_image" name="profile_image">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                  </div>
                                  <div class="text-danger"><strong id="profile_image_error"></strong></div>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="exampleInputEmail1">Address</label>
                            <textarea id="address" name="address" class="form-control form-control-sm" placeholder="Address">{{Auth::user()->address}}</textarea>
                            <div class="text-danger"><strong id="address_error"></strong></div>
                        </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-success" id="update_bttn">Submit</button>
                          </div>
                        </form>
                        <!-- /.card-body -->
                      </div>
                  
                  </div>
                  <!-- /.tab-pane -->

                
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 
  @endsection
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
     <script>
$(document).ready(function(){
   $('#update_details').submit(function(event){
        event.preventDefault();
        $("#update_bttn").prop('disabled', true);
        var formdata = new FormData($(this)[0]);
            $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache:false,
            data: formdata,
            success: function (response) {  
                $('.error').html('');
                if(response.success == true){
                    mdtoast(response.msg, { 
                        type: 'ewrty',
                        duration: 3000
                        });
                    
                    window.location.href='{{ route('delivery_profile') }}'
                }
            },error: function (jqXHR) {
                $("#update_bttn").prop('disabled', false);
              
                var errormsg = jQuery.parseJSON(jqXHR.responseText);
                $.each(errormsg.errors,function(key,value) {

                    $('#'+key+'_error').html(value);
                });
            }
        });
    });

    $('#adr').submit(function(event){
        event.preventDefault();
        //$("#update_bttn").prop('disabled', true);
        var formdata = new FormData($(this)[0]);
            $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache:false,
            data: formdata,
            success: function (response) {  
                $('.error').html('');
                if(response.success == true){
                    if(response.success == true){
                    mdtoast(response.msg, { 
                        type: 'ewrty',
                        duration: 3000
                        });
                        window.location.href='{{ route('delivery_profile') }}'
                }
                  
                }
            },error: function (jqXHR) {
               // $("#update_bttn").prop('disabled', false);
              
                var errormsg = jQuery.parseJSON(jqXHR.responseText);
                $.each(errormsg.errors,function(key,value) {

                    $('#'+key+'_error').html(value);
                });
            }
        });
    });















});

    </script>