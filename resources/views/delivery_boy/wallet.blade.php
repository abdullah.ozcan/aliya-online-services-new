@extends('delivery_boy.layouts.menu')
@section('title','wallet | E-Kirana')
@section('content')
<style>
    .re{
        border-radius: 42px !important;
    }
    .info-box {
    box-shadow: 0 0 1px rgba(0,0,0,.125), 0 1px 3px rgba(0,0,0,.2);
    border-radius: .25rem;
    background: #fff;
    display: -ms-flexbox;
    display: flex;
    margin-bottom: 5px !important;
    min-height: 80px;
    padding: .5rem;
    position: relative;
    width: 100%;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
   

    <!-- Main content -->
    <section class="content" style="
    padding-top: 14px;
    " >
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-info">
                        <span class="info-box-icon" style="font-size: 278%;padding-bottom: 45px;"><i class="fas fa-wallet"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text" style="font-size:150%">Wallet Amount</span><br>
                            <span class="info-box-number" style="font-size:150%"> ₹ {{$driver_wallet->wallet_ammount}}</span><br>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                            <span class="progress-description">
                               <a href="amount_withdrawal" style="color:white"> <i class="fas fa-arrow-right"></i> &nbsp Cash Withdrawal </a>
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>

            <h5 class="mb-2" style="padding-top: 10px;padding-bottom: 4px;margin-bottom: 0 !important;border-bottom: solid 1px #dfdfdf;">Transaction History</h5>
            @foreach($driver_wallet_transaction as $key=> $driver_wallet_transaction1)
            @if($key>0)
            @php($pd=$driver_wallet_transaction[$key-1]->created_at->format('d/m/Y'))
            @else
            @php($pd=$driver_wallet_transaction[$key]->created_at->format('d/m/Y'))
            @endif
            @php($cd=$driver_wallet_transaction1->created_at->format('d/m/Y'))
            
            
            @if($pd!=$cd)
            <div style="padding: 8px 0px 5px 1px;"> <b>{{$driver_wallet_transaction1->created_at->format('d M Y')}}<b></div>
            @endif
            @if($key==0)
            <div style="padding: 8px 0px 5px 1px;"> <b>{{$driver_wallet_transaction1->created_at->format('d M Y')}}</b></div>
            @endif
          
         
           
            <div class="row">
                
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box">
                        @if($driver_wallet_transaction1->tranasaction_type=='Credit')
                        <span class="info-box-icon bg-success "><i class="fas fa-rupee-sign"></i></span>
                        @else
                        <span class="info-box-icon bg-danger "><i class="fas fa-rupee-sign"></i></span>
                        @endif
                        <div class="info-box-content">
                        <div class="row">
                           
                            <div class="col-7" style="padding:0">
                                <div class="info-box-content">
                                   
                                    <span class="info-box-number">{{$driver_wallet_transaction1->remarks}}</span>
                                   
                                </div>
                            </div>
                           
                             <div class="col-5" style="padding:0">
                                <div class="info-box-content" style="float: right;">
                                   
                                    <span class="info-box-number"style="
                                    font-size: larger; "
                                >
                                @if($driver_wallet_transaction1->tranasaction_type=='Credit')
                                + ₹{{$driver_wallet_transaction1->transaction_amount}}
                                @else
                                - ₹{{$driver_wallet_transaction1->transaction_amount}}
                                @endif
                            
                            
                            </span>
                                </div>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar" style="width: 0%;"></div>
                        </div>
                        <div class="row">
                        <div class="col-4">
                            {{($driver_wallet_transaction1->created_at->format('H:i a'))}}
                        </div>
                      
                         <div class="col-8">
                            <span class="progress-description"  style="float: right;">
                               Closing Blalance: ₹{{$driver_wallet_transaction1->after_transaction_amount}}
                            </span>
                        </div>
                    </div>
                  

                    </div>
                    
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>
            @endforeach

        </div>
    </section>

    <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
        <i class="fas fa-chevron-up"></i>
    </a>
</div>

  <!-- /.content-wrapper -->


@endsection
  