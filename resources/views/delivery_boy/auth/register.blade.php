<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Register|E-Kirana</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('delivery_boy_assets/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('delivery_boy_assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('delivery_boy_assets/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>Register</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register a new membership</p>

    <form method="POST" id="" action="register" >
   
    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" >
        <div class="input-group mb-3">
          <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name"  value="{{ old('name') }}" placeholder="Full name">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
                                      @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                                 
        </div>
        <div class="text-danger"><strong class="error" id="name_error"></strong></div>
        <div class="input-group mb-3">
          <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                                  
        </div>
        <div class="text-danger"><strong class="error" id="email_error"></strong></div>
        <div class="input-group mb-3">
          <input type="number" class="form-control @error('mobile') is-invalid @enderror" name="mobile" id="mobile" value="{{ old('mobile') }}" placeholder="Phone">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone"></span>
            </div>
          </div>
                                   @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                                 
        </div>
        <div class="text-danger"><strong class="error" id="mobile_error"></strong></div>
        <div class="input-group mb-3">
          <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
                               @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                              
        </div>
        <div class="text-danger"><strong class="error" id="password_error"></strong></div>
        <input type="hidden" name="role" value="DELIVERY">
        <div class="input-group mb-3">
          <input type="password" class="form-control @error('email') is-invalid @enderror" name="password_confirmation"  id="password_confirmation" placeholder="Confirm password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <input type="hidden" value="" id="otp_value"> 

        <div id="otp2">
          <div class="input-group mb-3" >
            <input type="number" class="form-control" onkeyup="matchotp(this.value);" onchange="matchotp(this.value);" name="otp" id="otp" placeholder="otp">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-key"></span>
              </div>
            </div>
          </div>
          <span id="ps" style="color:red"></span>
        </div>


        <div class="row">
          <div class="col-12">
            <div class="icheck-primary">
              <input type="checkbox" id="terms" name="terms"  >
              <label for="terms">
               I agree to the <a href="#">Terms&Condition</a>
              </label>
            </div>
          </div>
          <div class="text-danger"><strong class="error" id="terms_error"></strong></div>
        </div>

          <div class="row">
          <!-- /.col -->
          <div class="col-12" >
              <div class="social-auth-links text-center" >
                    <a class="btn btn-block btn-primary" style="color:white" id="send-otp" onClick="sendOtp()">
                    Send Otp
                    </a>
            </div>
         </div>
        
         <div class="col-6">
            <button  class="btn btn-block btn-primary" style="color:white" id="as">
             Sign Up
            </button>
        </div> 

        <div class="col-6">
          <a  class="btn btn-block btn-info" id="resend-otp" style="color:white" onClick="sendOtp()">
            Resend Otp
          </a>
        </div> 
     
      </div>




          <!-- /.col -->
       
      </form>

     

      <a href="{{route('delivery_login')}}" class="text-center">I already have a membership</a>

    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="{{ asset('delivery_boy_assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('delivery_boy_assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('delivery_boy_assets/dist/js/adminlte.min.js')}}"></script>
</body>
</html>

<!--script start-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
  $(document).ready(function(){
    
  
            document.getElementById("as").style.display = "none";
            document.getElementById("otp2").style.display = "none";
            document.getElementById("resend-otp").style.display = "none";

  });

      // $('#myForm').submit(function(event){
      //   event.preventDefault();
      //   var formdata = new FormData($(this)[0]);


      function sendOtp()
        {
        
          var name =$('#name').val();
          var email =$('#email').val();
          var mobile =$('#mobile').val();
          var password =$('#password').val();
          var password_confirmation =$('#password_confirmation').val();
          var terms =document.getElementById('terms').checked;
          var token =$('#_token').val();
        
         
            $.ajax({
            url: "{{ route('register_otp') }}",
            type: 'POST',
            data: {name:name,_token:token,email:email,mobile:mobile,password:password,password_confirmation:password_confirmation,terms:terms},
            success: function (response) {  
              
                if(response.success == true){
                  $('.error').html('');
                   document.getElementById("send-otp").style.display = "none";
                   document.getElementById("resend-otp").style.display = "block";
                   document.getElementById("otp2").style.display = "block";
                   document.getElementById("otp_value").value=response.otp11;
                   document.getElementById("as").style.display = "block";
                   document.getElementById("as").disabled = true; 

                    mdtoast(response.msg, { 
                        type: 'ewrty',
                        duration: 3000
                        });
                }

            },error: function (jqXHR) {
                $("#update_bttn").prop('disabled', false);
              
                var errormsg = jQuery.parseJSON(jqXHR.responseText);
                $.each(errormsg.errors,function(key,value) {
                    $('#'+key+'_error').html(value);
                });
            }
        });
       
        }
          
   


  function matchotp(otp){ 
            var orotp=document.getElementById("otp_value").value
            var len=otp.length;
            if(len==6)
            {
            if(otp==orotp)
            {
                document.getElementById("as").disabled = false;
                document.getElementById("ps").innerHTML = "";
            }else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";
                document.getElementById("as").disabled = true;
            }
            }
            else if(len>6)
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
                document.getElementById("as").disabled = true;   
            }
            else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
                document.getElementById("as").disabled = true;  
              
            }
      }

</script>
