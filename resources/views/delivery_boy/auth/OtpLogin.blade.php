<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Login|E-Kirana</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('delivery_boy_assets/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('delivery_boy_assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('delivery_boy_assets/dist/css/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Login</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="{{route('loginWithOtp_driver')}}" method="post" enctype="multipart/form-data">
      @csrf
        <div class="input-group mb-3">
          <input type="number" class="form-control" id="mobile" name="mobile" placeholder="Phone No">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone"></span>
            </div>
          </div>
        </div>
      
        <input type="hidden" value="" id="otp_value">
        <input type="hidden" name="role" value="DELIVERY" id="role">
        <div id="otp2"> 
        <div class="input-group mb-3" >
          <input type="otp" class="form-control" name="otp" onkeyup="matchotp(this.value);" onchange="matchotp(this.value);"  placeholder="OTP">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        
        </div>
        <span id="ps" style="color:red"></span>
        </div>
        <div class="row">
         
          <!-- /.col -->
          <div class="col-12">
            <a type="submit" class="btn btn-primary btn-block" style="color:white" id="send-otp" onClick="sendOtp()">Send Otp</a>
          </div>
          
          <!-- /.col -->
        </div>
        <div class="row">
        <div class="col-6">
            <button type="submit" class="btn btn-primary btn-block" id="as" style="color:white">Login</button>
          </div>
        <div class="col-6">
            <a type="submit" class="btn btn-info btn-block" id="resend-otp" onclick="sendOtp()" style="color:white">Resend Otp</a>
          </div>

        </div>

      </form>

      <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="{{route('delivery_login')}}" class="btn btn-block btn-danger">
           Login With Email
        </a>
      </div>
      <!-- /.social-auth-links -->

     
      <p class="mb-0">
        <a href="{{route('delivery_register')}}" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('delivery_boy_assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('delivery_boy_assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('delivery_boy_assets/dist/js/adminlte.min.js')}}"></script>

</body>
</html>


<!-- script section -->
<script>
            document.getElementById("as").style.display = "none";
            document.getElementById("otp2").style.display = "none";
            document.getElementById("resend-otp").style.display = "none";
            //  document.getElementById("send-otp").style.display = "none";
            

 function sendOtp() {
   
       
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });

       $.ajax( {
           type:'post',
           url:"{{route('delivery_sendOtp')}}",
           data: {'mobile': $('#mobile').val()},
           success:function(data) {
           
          
               var data1 = data.split("|"); 
               if(data1[0] == 1){
                   
                   document.getElementById("send-otp").style.display = "none";
                   document.getElementById("resend-otp").style.display = "block";
                   document.getElementById("otp2").style.display = "block";
                   document.getElementById("otp_value").value=data1[1];
                   document.getElementById("as").style.display = "block";
                   document.getElementById("as").disabled = true;   
               }
               
               else if(data1[0] == 2)
               {
                swal("Your Approval is Canceled. Please Contact to the admin.");
               }
               else{
                   swal("Mobile No not found!");
               }

           },
           error:function () {
               console.log('error');
           }
       });
   }


       function matchotp(otp){ 
            
            var orotp=document.getElementById("otp_value").value
            var len=otp.length;
            if(len==6)
            {
            if(otp==orotp)
            {
                document.getElementById("as").disabled = false;
                document.getElementById("ps").innerHTML = "";
            }else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";
                document.getElementById("as").disabled = true;
            }
            }
            else if(len>6)
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
                document.getElementById("as").disabled = true;   
            }
            else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
                document.getElementById("as").disabled = true;  
              
            }
      }

</script>
