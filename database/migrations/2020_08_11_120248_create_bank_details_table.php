<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_details', function (Blueprint $table) {
            $table->id();
            $table->string('a_h_name')->default(0);
            $table->string('b_name')->default(0);
            $table->string('a_no')->default(0);
            $table->string('ifcs_code')->default(0);
            $table->string('upi_id')->default(0);
            $table->string('qr_code')->default(0);
            $table->string('delivery_id')->default(0); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_details');
    }
}
