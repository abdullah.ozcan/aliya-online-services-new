<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockTrasanctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_trasanctions', function (Blueprint $table) {
            $table->bigIncrements('stock_trasanctions_id');
            $table->foreignId('product_id')->default(0);
            $table->integer('quantity')->default(0);
            $table->string('type')->nullable();
            $table->integer('total_quantity')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_trasanctions');
    }
}
