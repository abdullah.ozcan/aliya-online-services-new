<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('product_id');
            $table->string('product_code')->nullable();
            $table->string('product_name')->nullable();
            $table->foreignId('cat_id')->default(0);
            $table->foreignId('sub_cat_id')->default(0);
            $table->foreignId('sub_sub_cat_id')->default(0);
            $table->longText('description')->nullable();
            $table->foreignId('brand_id')->default(0);
            $table->string('hsn_code')->nullable();
       
            $table->integer('view_count')->default(0);
            $table->integer('total_review')->default(0);
            $table->string('review')->default('0.0');
            $table->string('active_status')->default('YES');


            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
