<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdradalRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdradal_requests', function (Blueprint $table) {
            $table->bigIncrements('withdrawal_request_id');
            $table->bigInteger('delivery_id')->nullable();
            $table->String('withdrawal_amount')->nullable();
            $table->String('withdrawal_status')->default('NO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdradal_requests');
    }
}
