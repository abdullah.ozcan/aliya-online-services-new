<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('booking_id');


            $table->integer('customer_id')->default(0);
            $table->String('coupon_code')->nullable();
            $table->String('price')->default(0);
            $table->String('total_price')->default(0);
            $table->String('cart_total')->default(0);
            $table->String('delivery_charge')->default(0);
            $table->String('wallet_amount')->default(0);
            $table->String('coupon_discount')->default(0);
            $table->String('order_id')->nullable();
            $table->String('payment_type')->nullable();
            $table->String('razorpay_payment_id')->nullable();
            $table->String('payment_status')->default('NO');
            $table->String('notify_status')->default('unseen');
            $table->String('wallet_payment_status')->default('NO');


            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
