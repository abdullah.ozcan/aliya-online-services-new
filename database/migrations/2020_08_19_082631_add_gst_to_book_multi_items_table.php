<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGstToBookMultiItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_multi_items', function (Blueprint $table) {
            $table->integer('gst')->default(0);
            $table->integer('delivery_boy_id')->default(0);
            $table->integer('return_delivery_boy_id')->default(0);
            $table->string('return_date')->nullable();
      
              });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('book_multi_items', function (Blueprint $table) {
            $table->dropColumn('gst');
            $table->dropColumn('delivery_boy_id');
            $table->dropColumn('return_delivery_boy_id');
            $table->dropColumn('return_date');
        });
    }
}
