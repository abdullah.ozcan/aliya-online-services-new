<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGroupIdToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('group_id')->default(0);
            $table->integer('weight')->default(0);
            $table->integer('mrp')->default(0);
            $table->integer('selling_price')->default(0);
            $table->integer('total_stock')->default(0);
            $table->integer('available_stock')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('group_id');
            $table->dropColumn('weight');
            $table->dropColumn('mrp');
            $table->dropColumn('selling_price');
            $table->dropColumn('total_stock');
            $table->dropColumn('available_stock');

        });
    }
}
