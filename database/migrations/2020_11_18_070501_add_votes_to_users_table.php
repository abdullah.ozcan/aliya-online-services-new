<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVotesToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->string('pin_no')->default(0);
            $table->string('companey_name')->default(0);
            $table->string('pan_no')->default(0);
            $table->string('aadhar_no')->default(0);
            $table->string('gst_no')->default(0);
            $table->string('aadhar_image')->default(0);
            $table->string('pan_image')->default(0);

            $table->string('vendor_sign')->default(0);
            $table->string('companey_logo')->default(0);           



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('pin_no');
            $table->dropColumn('companey_name');
            $table->dropColumn('pan_no');
            $table->dropColumn('aadhar_no');

            $table->dropColumn('gst_no');
            $table->dropColumn('aadhar_image');
            $table->dropColumn('pan_image');

            $table->dropColumn('vendor_sign');
            $table->dropColumn('companey_logo');

        });
    }
}
